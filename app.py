from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"


@app.route("/add_two_nums",methods = ["POST"])
def add_two_nums():
  datadict = request.get_json()
  # return jsonify(datadict)
  
  x = datadict.get("x")
  y = datadict.get("y")
  
  z = x + y

  retJSON = {
    "z" : z
  }
    
  return jsonify(retJSON)

if __name__ == "__main__":
  app.run(debug=True)
